var express = require('express');
const {requiresAuth} = require("express-openid-connect");
var router = express.Router();

var users = [];


router.get('/:latitude?/:longitude?/', requiresAuth(), function (req, res, next) {
    req.user = {
        isAuthenticated : req.oidc.isAuthenticated()
    };
    if (req.user.isAuthenticated) {
        req.user = {...req.user,... req.oidc.user};
    }
    if (users.filter(user => user.email === req.oidc.user.email).length === 0 && req.params.latitude!=null && req.params.longitude!=null) {
        let user = {...req.oidc.user,latitude: req.params.latitude,longitude: req.params.longitude}
        users.push(user);
        while (users.length > 5) users.shift();
    }
    res.render('users',{ users: users, user: req.user});
});

module.exports = router;
