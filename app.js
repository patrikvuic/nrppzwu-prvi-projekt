var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var https = require('https')
var fs = require('fs');
const { auth, requiresAuth } = require('express-openid-connect')

var store = require('store')
store.set('users',[]);


const dotenv = require('dotenv');
dotenv.config();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const port = process.env.PORT || 3000


const config = {
    authRequired : false,
    idpLogout : true, //login not only from the app, but also from identity provider
    secret: process.env.SECRET,
    baseURL: process.env.APP_URL || `https://localhost:${port}`,
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: 'https://dev-yh4nhvbf.us.auth0.com',
    clientSecret: process.env.CLIENT_SECRET,
    authorizationParams: {
        response_type: 'code'
        //scope: "openid profile email"
    }
};

app.use(auth(config))

app.use('/', indexRouter);
app.use('/private', usersRouter);



process.env.PORT ?
    app.listen(port, function () {
      console.log(`Server running at http://localhost:${port}/`);
    }) :
    https.createServer({
      key: fs.readFileSync('server.key'),
      cert: fs.readFileSync('server.cert')
        },app).listen(port, function () {
      console.log(`Server running at https://localhost:${port}/`);
    })

