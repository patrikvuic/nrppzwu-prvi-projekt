
function geoFindMe() {

    const status = document.querySelector('#status');
    const mapLink = document.querySelector('#map-link');
    const privateMap = document.querySelector('#private-map')

    mapLink.href = '';
    mapLink.textContent = '';

    async function success(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        status.textContent = '';
        mapLink.href = `https://www.openstreetmap.org/#map=18/${latitude}/${longitude}`;
        mapLink.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;


        if(isAuthenticated){
           privateMap.href = `/private/${latitude}/${longitude}`;
        }else{
            privateMap.href = `/private`;
        }

        let mapOptions = {
            center: [latitude, longitude],
            zoom: 13
        }

        let map = new L.Map('map', mapOptions);
        let layer = new L.TileLayer(`https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`);
        map.addLayer(layer);

        let marker = new L.Marker([latitude, longitude]);
        marker.addTo(map);
    }

    function error() {
        status.textContent = 'Unable to retrieve your location';
    }

    if (!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }


}
window.onload = () => {
    geoFindMe();
}
