window.onload = () => {
    let userCurr = users.filter(userIt => userIt.email ===user.email)[0];
    let mapOptions;
    if(userCurr== null || userCurr.latitude ==null || userCurr.longitude==null){
        mapOptions = {
            center: [users[0].latitude, users[0].longitude],
            zoom: 13
        }
    }else{
        mapOptions = {
            center: [userCurr.latitude, userCurr.longitude],
            zoom: 13
        }
    }

    let map = new L.Map('map', mapOptions);
    let layer = new L.TileLayer(`https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`);
    map.addLayer(layer);

    users.forEach(userLoop => {

        let marker = new L.Marker([userLoop.latitude, userLoop.longitude]);
        marker.bindPopup(`<p>Korisnik: ${userLoop.email}</br>Vrijeme prijave: ${new Date(userLoop.updated_at)}</p>`).openPopup();
        marker.addTo(map);
    });
};